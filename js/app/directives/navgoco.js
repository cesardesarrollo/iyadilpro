angular
    .module('proulex')
    .directive('navToggleSub', navToggleSub);

function navToggleSub() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.navgoco({
                caretHtml: '<i class="fa fa-fw fa-chevron-down has-submenu"></i>',
                accordion: true
            });
        }
    };
};