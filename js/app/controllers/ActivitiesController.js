proulex.controller('ActivitiesController', ['$scope',
  function($scope) {




    $scope.activities = [
        {
            id: 1,
            name: 'Preguntas Opción Múltiple 1',
            description: 'Descripción',
            selected: false
        },
        {
            id: 2,
            name: 'Preguntas Opción Múltiple 2',
            description: 'Descripción',
            selected: false
        },
        {
            id: 3,
            name: 'Preguntas Opción Múltiple 3',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        },
        {
            id: 4,
            name: 'Preguntas Opción Múltiple 4',
            description: 'Descripción',
            selected: false
        }
    ];
    
    $scope.removeActivities = function() {
        $scope.activities = $scope.activities.filter(function(activity){
            return !activity.selected;
        });        
    };
    
    $scope.copyActivities = function() {
        $scope.activities.filter(function(activity){
            if (activity.selected) {
                activity.selected = false;
                $scope.activities.push({
                    name: "Copia de " + activity.name,
                    id: Math.random() * 1001,
                    selected: true
                });
            }
        });        
    };
  }
]);