
proulex.controller('NewActivityController',['$scope','$http','dataModel','dataModelActivityType','Data','toaster','$uibModal','$log',function($scope,$http,dataModel,dataModelActivityType,Data,toaster,$uibModal,$log) {

    $scope.formData = {};
    $scope.data             = dataModel.data;   /* Obtiene JSON desde DataModel */
    $scope.dataActivityType = dataModelActivityType.data;   /* Obtiene JSON desde dataModelActivityType con datos de categorias de actividad */
    $scope.formData.question = "";
    $scope.formData.answer = "";
    $scope.formData.selectedPregunta = $scope.data[0].activity.questions[0];
    $scope.errorNewPregunta = false;
    $scope.errorNewRespuesta = false;

    $scope.validRespuesta = function(item,answers) {
        item.is_correct = true;
    };

    $scope.invalidRespuesta = function(item) {
        item.is_correct = false;
    };

    $scope.togglePregunta = function(pregunta) {
        pregunta.expanded = !pregunta.expanded;
    };

    $scope.addPregunta = function() {
        if ($scope.formData.question) {
            var newPregunta = { 
                question: $scope.formData.question, 
                expanded: false, 
                question_type: $scope.formData.question_type, 
                attemps: $scope.formData.attemps,  
                help_text: $scope.formData.help_text,  
                timeout: $scope.formData.timeout 
            };
            newPregunta.answers = [];
            $scope.data[0].activity.questions.push(newPregunta);
            $scope.errorNewPregunta = false;
            
            $scope.formData = {};
        } else {
          $scope.errorNewPregunta = true;
        }
    };

    $scope.addRespuesta = function() {
        if ($scope.formData.answer) {
            var newRespuesta = { 
                answer: $scope.formData.answer, 
                is_correct: false 
            };
            $scope.formData.selectedPregunta.answers.push(newRespuesta);
            $scope.formData.selectedPregunta.expanded=true;
            $scope.errorNewRespuesta = false;
            $scope.formData.answer = "";
        } else {
            $scope.errorNewRespuesta = true;
        }
    };

    $scope.deletePregunta = function(data, item) {
        data.splice(data.indexOf(item), 1);
    };

    $scope.deleteRespuesta = function(pregunta, item) {
        pregunta.answers.splice(pregunta.answers.indexOf(item), 1);
    };

    $scope.limpiarPregunta = function() {
        $scope.formData = {};
    };

    $scope.limpiarRespuesta = function() {
        $scope.formData.answer = "";
    };

    $scope.processForm = function() {
        //alert('procesando formulario..'); 
    };

    $scope.guardarParametros = function() {
        Data.post("activities",$scope.data[0].activity).then(function(result) {
            console.log(result);

            var toast = {};
            toast.status = (result.status==201)?"success":"error";
            toast.message = (result.status==201)?"¡Se ha creado la actividad " + result.data.activity.id + " correctamente!":"Ha ocurrido un error";
            toaster.pop(toast.status, "",toast.message, 5000, 'trustedHtml');     
            $scope.formData = {};
        });
    };

    /* Datos generales */
    Data.get("modules").then(function(result) {
        $scope.modules = result.modules;
        
    });
    Data.get("lessons").then(function(result) {
        $scope.lessons = result.lessons;
        
    });
    Data.get("units").then(function(result) {
        $scope.units = result.units;
        
    });
    Data.get("levels").then(function(result) {
        $scope.levels = result.levels;
        
    });

    /* Media Modal */
    Data.get("media",{"where":{"mime_type":"image/jpeg"}}).then(function(result) {
        $scope.items = result;
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'seleccionMultimedia',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.formData.answer = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    });
}])
.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items) {
  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});



