'use strict';

/* App Module */
var app = angular.module('app', [
    'ui.router',
    'ngAnimate', 
    'toaster',
    'angular-loading-bar',
    'proulexFilters',
    'angular-storage',
    'ui.bootstrap',
    'ngTouch'

])
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'views/home.html',
            controller: 'ActivitiesController'
        })
        .state('activities', {
            url: '/activities',
            templateUrl: 'views/activities.html',
            controller: 'ActivitiesController'
        })
        .state('newActivity', {
            url: '/newActivity',
            templateUrl: 'views/forms/newActivity.html',
            controller: 'NewActivityController'
        })
        
    $urlRouterProvider.otherwise('/');   
})
.controller('loginController',['$scope','$http','Data','toaster','store', '$location',function($scope,$http,Data,toaster,store, $location) {

    $scope.logear = function(){
        Data.post("Users/login",$scope.user).then(function(result) {
            var toast = {};
            toast.status = (result.status==200)?"success":"error";
            toast.message = (result.status==200)?"¡Logeado correctamente!":"Ha ocurrido un error";
            toaster.pop(toast.status, "",toast.message, 3000, 'trustedHtml');                  
            store.set('token', result.data);
            window.open("http://localhost:3001/","_self");
        });    

    };
}])
.run(["$rootScope", 'store', '$location', function($rootScope, store, $location) {
    $rootScope.$on('$locationChangeStart', function(event, next) {
        var token = store.get("token") || null;
        if (!token)
        window.open("http://localhost:3001/login#/","_self");

    });
}]);



