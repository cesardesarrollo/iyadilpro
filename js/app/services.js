'use strict';

/* Services */

var phonecatServices = angular.module('phonecatServices', ['ngResource']);

phonecatServices.factory('Phone', ['$resource',
  function($resource){
    return $resource('phones/:phoneId.json', {}, {
      query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
    });
  }]);


proulex.factory('dataModel', function() {
  return new DataModel();
});

proulex.factory('dataModelActivityType', function() {
  return new DataModelActivityType();
});

proulex.factory("Data", ['$http', 'toaster',
    function ($http, toaster) {

        var serviceBase = 'http://54.210.213.150:3000/api/';  

        var obj = {};
    	var result = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {

                /*
				result.status = (results.status==200)?"success":"error";
				result.message = (results.status==200)?"¡Datos obtenidos correctamente!":"Ha ocurrido un error";
            	obj.toast(result);
                */

                return results.data;
            });
        };
        obj.post = function (q, object) {

            return $http.post(serviceBase + q, object).then(function (results) {
                /*
				result.status = (results.status==201)?"success":"error";
				result.message = (results.status==201)?"¡Operación exitosa!":"Ha ocurrido un error";
            	obj.toast(result);
                */

                return results;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };

        return obj;
}]);
