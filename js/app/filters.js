'use strict';

/* Filters */

angular.module('iyadilproFilters', [])
.filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
})
.filter('cutActivityName', function() {
  return function(input, len) {
    return input.toString().length > len ? input.toString().substr(0, len) + ' ...' : input;
  };
});
