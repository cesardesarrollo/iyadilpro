<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.png">
    <!-- non-retina iPhone pre iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon57.png" sizes="57x57">
    <!-- non-retina iPad pre iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon72.png" sizes="72x72">
    <!-- non-retina iPad iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon76.png" sizes="76x76">
    <!-- retina iPhone pre iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon114.png" sizes="114x114">
    <!-- retina iPhone iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon120.png" sizes="120x120">
    <!-- retina iPad pre iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon144.png" sizes="144x144">
    <!-- retina iPad iOS 7 -->
    <link rel="apple-touch-icon" href="images/favicons/icon152.png" sizes="152x152">
    <title>Semillas Iyadilpro y Ya, Semillas de alta calidad </title>
    <meta name="description" content="Semillas de alta calidad
Investigación producción acondicionamiento comercialización de semillas
Híbridos, Prov. genético de INIFAP CIMMYT COLPOS y Privadas">
    <meta name="keywords" content="Semillas,Investigación de semillas,producción de semillas,acondicionamiento de semillas,comercialización de semillas,proveedor de híbridos, proveedor genético de INIFAP,proveedor de CIMMYT,proveedor de COLPOS ,proveedor genético de empresas privadas">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome-4.4.0/css/font-awesome.min.css">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav id="primary">
      <ul>
        <li>
          <h1>Inicio</h1>
          <a class="inicio" href="#inicio"></a>
        </li>
        <li>
          <h1>Quienes Somos</h1>
          <a class="quienessomos" href="#quienessomos"></a>
        </li>
        <li>
          <h1>Cultivos</h1>
          <a class="cultivos" href="#cultivos"></a>
        </li>
        <li>
          <h1>Presencia</h1>
          <a class="Presencia" href="#Presencia"></a>
        </li>
        <li>
          <h1>Galería</h1>
          <a class="galeria" href="#galeria"></a>
        </li>
        <li>
          <h1>Contacto</h1>
          <a class="contacto" href="#contacto"></a>
        </li>
        <li>
          <h1>Ubicación</h1>
          <a class="ubicacion" href="#ubicacion"></a>
        </li>
      </ul>
    </nav>
    
    <div class="cover-container">
    </div>
    <div class="site-wrapper" id="inicio">
      <div class="site-wrapper-inner animated fadeIn fill">
        <nav id="navbar" class="navbar navbar-fixed-top navbar-default visible-xs">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#inicio"><i class="fa fa-home"></i><span class="hidden">Semillas Iyadilpro</span></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><a href="#quienessomos">Quienes somos</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cultivos <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                      <a href="#maices">Maices</a>
                      <ul class="dropdown-menu">
                        <li><a href="#maiz-blancos">Maiz Blanco</a></li>
                        <li><a href="#maiz-amarillos">Maiz Amarillo</a></li>
                      </ul>
                    </li>
                    <li><a href="#trigo">Trigo</a></li>
                    <li><a href="#avena">Avena</a></li>
                    <li><a href="#cartamo">Cártamo</a></li>
                  </ul>
                </li>
                <li><a href="#Presencia">Presencia</a></li>
                <li><a href="#galeria">Galería</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#contacto">Contacto</a></li>
                <li><a href="#ubicacion">Ubicación</a></li>
              </ul>
              </div><!--/.nav-collapse -->
              </div><!--/.container-fluid -->
            </nav>
            <div id="myCarousel" class="carousel slide ">
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>
              <div class="carousel-inner">
                <div class="active item">
                  <div class="fill" style="background-image:url('images/img3.jpg');">
                    <div class="container">
                      <div class="logoCenter visible-lg">
                        <img class="animated pulse img-responsive" src="images/logo160x315blanco.png" alt="Logotipo Semillas Iyadilpro">
                      </div>
                      <h1 class="animated bounceInUp etiqueta">
                      <span class="txt-white">EL MAÍZ MÁS MEXICANO</span></h1>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="fill" style="background-image:url('images/img1.jpg');">
                    <div class="container">
                      <div class="logoCenter visible-lg">
                        <img class="animated pulse img-responsive" src="images/logo160x315blanco.png" alt="Logotipo Semillas Iyadilpro">
                      </div>
                      <h1 class="animated bounceInUp etiqueta">DESARROLLAMOS SOLUCIONES INNOVADORAS<br/>
                      <span>COMPROMETIDAS CON EL CUIDADO</span><br/>
                      DEL MEDIO AMBIENTE</h1>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="fill" style="background-image:url('images/img2.jpg');">
                    <div class="container">
                      <div class="logoCenter visible-lg">
                        <img class="animated pulse img-responsive" src="images/logo160x315blanco.png" alt="Logotipo Semillas Iyadilpro">
                      </div>
                      <h1 class="animated bounceInUp etiqueta">SOMOS UN EQUIPO QUE SE PREOCUPA POR EL MEJORAMIENTO<br/>
                      <span>Y BIENESTAR DEL CAMPO MEXICANO</span><br/>
                      Y MÁS ALLÁ DE SUS FRONTERAS</h1>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="fill" style="background-image:url('images/img4.jpg');">
                    <div class="container">
                      <div class="logoCenter visible-lg">
                        <img class="animated pulse img-responsive" src="images/logo160x315blanco.png" alt="Logotipo Semillas Iyadilpro">
                      </div>
                      <h1 class="animated bounceInUp etiqueta"><span>24 AÑOS HACIENDO LAS MEJORES SEMILLAS PARA SIEMBRA</span><br/>
                      POR LA SOBERANÍA, LA DIGNIDAD Y EL PROGRESO DE NUESTRO CAMPO
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
              <div class="pull-center">
                <a class="carousel-control left" href="#myCarousel" data-slide="prev"></a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"></a>
              </div>
            </div>
            <div class="row" id="opciones">
              <div class="container hidden-xs">
                <div class="col-md-3 col-sm-3 opcion">
                  <a href="#quienessomos">
                    <img class="wow shake img-responsive img-circle" src="images/buttons/btn1.jpg" alt="QUIENES SOMOS">
                  <h3>QUIENES SOMOS</h3></a>
                </div>
                <div class="col-md-3 col-sm-3 opcion">
                  <a href="#cultivos">
                    <img class="wow shake img-responsive img-circle" src="images/buttons/btn2.jpg" alt="CULTIVOS">
                  <h3>CULTIVOS</h3></a>
                </div>
                <div class="col-md-3 col-sm-3 opcion">
                  <a href="#Presencia">
                    <img class="wow shake img-responsive img-circle" src="images/buttons/btn3.jpg" alt="PRESENCIA">
                  <h3>PRESENCIA</h3></a>
                </div>
                <div class="col-md-3 col-sm-3 opcion">
                  <a href="#galeria">
                    <img class="wow shake img-responsive img-circle" src="images/buttons/btn4.jpg" alt="GALERIA">
                  <h3>GALERIA</h3></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row franjaPadding" id="quienessomos">
          <div class="col-md-12 franja wow pulse">
            <h1>QUIENES SOMOS</h1>
          </div>
        </div>
        <div id="myquienessomosCarrusel" class="carousel slide"  data-interval="false">
          <ol class="carousel-indicators container pull-center">
            <li data-target="#myquienessomosCarrusel" data-slide-to="0" class="active"> MISIÓN </li>
            <li data-target="#myquienessomosCarrusel" data-slide-to="1"> VISIÓN </li>
          </ol>
          <div class="carousel-inner">
            <div class="active item">
              <div class="fill" style="background-image:url('images/quienes-somos/mision.jpg');">
                <div class="container">
                  <p class="lead animated bounceInLeft etiqueta">
                  Continuar trabajando para el desarrollo productivo permanente y ser <strong>líder en el ramo agrícola;</strong> que signifiquemos una herramienta básica en el <strong>desarrollo eficiente de la agricultura Nacional</strong> y más allá de nuestras fronteras..</p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="fill" style="background-image:url('images/quienes-somos/vision.jpg');">
                <div class="container">
                  <p class="lead animated bounceInRight etiquetaDown">
                  Ser reconocida en el campo agrícola como una de las empresas que proporciona <strong>soluciones a la producción, calidad de vida y compromiso con el medio ambiente.</strong></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="featurette-heading">HISTORIA</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <p class="texto-justificado"><strong>IYADILPRO</strong> es la combinación de la experiencia en la investigación, producción, acondicionamiento y comercialización de semillas para el campo mexicano y más allá de nuestras fronteras.</p>
              <p class="texto-justificado">Su fundador el <strong>Ing. Alejandro Castellanos Sánchez</strong> después de su trayectoria por tres grandes instituciones Colpos, Inifap y Pronase emprende su camino de manera independiente en 1991 ubicándose en la región de la <strong>Ciénega de Chapala</strong> zona de alto rendimiento y cercanía al agricultor.
              </p>
            </div>
            <div class="col-md-4">
              <p class="texto-justificado">
              Desde entonces nos hemos caracterizado por ofrecer productos con alta calidad con un sentido social, mejores híbridos al menor precio del mercado.  </p>
              <p class="texto-justificado"> Nuestros principales proveedores de material genético es el <strong>INIFAP</strong> (Instituto Nacional de Investigaciones Forestales, Agrícolas y Pecuarias), <strong>CIMMYT</strong> (Centro Internacional de Mejoramiento de Maíz y Trigo) y <strong>COLPOS</strong> (Colegio de Posgraduados), sin embargo contamos con participación de empresas e investigadores privados que nos permiten
              introducir en el mercado nuevos y mejores híbridos.</p>
            </div>
            <div class="col-md-4">
              <p class="texto-justificado"><strong>Iyadilpro</strong> pone a su disposición toda experiencia y profesionalismo para proporcionar semillas de la más alta calidad a precios alcanzables para la mayoría de los productores en diferentes condiciones de la república mexicana principalmente Jalisco, San Luis Potosí, Guanajuato, Michoacán, Oaxaca, Guerrero, Chiapas, Colima, Sinaloa, entre otros. Con un alto sentido de responsabilidad social, económica y ambiental.</p>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-6">
              <img data-wow-duration="0.15s" class="wow rollIn img-responsive" src="images/quienes-somos/franja/img1.jpg" alt="comercialización de semillas para el campo">
            </div>
            <div class="col-md-3 col-sm-6">
              <img data-wow-duration="0.35s" class="wow rollIn img-responsive" src="images/quienes-somos/franja/img2.jpg" alt="experiencia en la investigación y comercialización de semillas">
            </div>
            <div class="col-md-3 col-sm-6">
              <img data-wow-duration="0.65s" class="wow rollIn img-responsive" src="images/quienes-somos/franja/img3.JPG" alt="semillas y productos con alta calidad">
            </div>
            <div class="col-md-3 col-sm-6">
              <img data-wow-duration="0.85s" class="wow rollIn img-responsive" src="images/quienes-somos/franja/img4.JPG" alt="semillas híbridas al menor precio del mercado">
            </div>
          </div>
        </div>
        <div class="back">
          <div class="row franjaPadding" id="cultivos">
            <div class="col-md-12 franja wow pulse">
              <h1>CULTIVOS</h1>
            </div>
          </div>
        </div>
        <div class="site-wrapper">
          <div class="site-wrapper-inner portada-maiz-blancos wow fadeIn parallax" data-stellar-background-ratio="0">
            <div class="container">
              <p class="texto-justificado txt-black">Semillas de alta calidad a precios alcanzables.<br>
                Investigación, producción, acondicionamiento y comercialización de semillas para el campo mexicano.<br>
                Proveedores de material genético INIFAP, CIMMYT, COLPOS.<br>
                Participación de empresas e investigadores privados para introducir en el mercado nuevos y mejores híbridos.
              </p>
            </div>
          </div>
        </div>
        <div class="back">
          <div class="row franjaPadding" id="maices">
            <div class="col-md-12 franja wow pulse">
              <h1>HÍBRIDOS DE MAÍZ <span> (Zea mays L.)</span></h1>
            </div>
          </div>
        </div>
        <div class="site-wrapper hidden-xs">
          <div class="site-wrapper-inner portada-maiz-amarillos wow fadeIn parallax" data-stellar-background-ratio="0">
            <div class="cover-container">
            </div>
          </div>
        </div>
        <div class="site-wrapper full">
          <div class="site-wrapper-inner">
            <h1 class="categoria franjaPadding" id="maiz-blancos">Maices Blancos</h1>
            <a href="#">Descargar PDF <img src="images/cultivos/icon_pdf.png" alt="Descargar catálogo de maices blancos" width="32" height="40" /></a>
            <div class="container franjaPadding">
              <div class="row">
                <div class="col-md-12 bg-verde">
                  <h2 class="semilla-nombre">H-377</h2><!---->
                </div>
              </div>
              <div class="row cultivo-item">
                <div class="col-md-9">
                  <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/H-377-CATALOGO-2016.jpg" alt="Maiz H-377" />
                  <div class="row">
                    <div class="col-md-3 col-sm-3 bg-gris uno">
                      <p>Híbrido blancocremoso dentado.</p>
                    </div>
                    <div class="col-md-3 col-sm-3 bg-gris dos">
                      <p>Buena adapatación 900-1850 m.s.n.m.</p>
                    </div>
                    <div class="col-md-3 col-sm-3 bg-gris uno">
                      <p>Punta de Riego  y buen temporal.</p>
                    </div>
                    <div class="col-md-3 col-sm-3 bg-gris dos">
                      <p>Buena sanidad  de planta y mazorca.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <img src="images/mapas/MAIZ BLANCO H377.png" class="img-responsive" alt="Mapa Maiz H-377" />
                  <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Edo. de México, Guerrero, Jalisco, Michoacán, Oaxaca, San Luis Potosí, Sinaloa, Tamaulipas y Zacatecas.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="site-wrapper full">
          <div class="site-wrapper-inner">
            <div class="container">
              <div class="row">
                <div class="col-md-12 bg-verde">
                  <h2 class="semilla-nombre">H-318</h2><!---->
                </div>
              </div>
              <div class="row cultivo-item">
                <div class="col-md-9">
                  <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/H-318-catalogo-2016.jpg" alt="Maiz H-318" />
                  <div class="row">
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Híbrido blancocremoso.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris dos">
                      <p>Adaptación a regiones tropicales y subtropicales 800-1900 m.s.n.m.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Ciclo intermedio.</p>
                    </div>
                    <div class="col-md-2 col-sm-6 bg-gris dos">
                      <p>Maíz con doble propósito grano y forraje.</p>
                    </div>
                    <div class="col-md-4 col-sm-6 bg-gris uno">
                      <p>Buena cobertura de mazorca y tolerante al acame.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <img src="images/mapas/MAIZ BLANCO H318.png" class="img-responsive" alt="Mapa Maiz H-318" />
                  <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Edo. de México, Guerrero, Jalisco, Michoacán, Oaxaca, San Luis Potosí, Sinaloa, Tamaulipas y Zacatecas.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="site-wrapper full">
          <div class="site-wrapper-inner">
            <div class="container">
              <div class="row">
                <div class="col-md-12 bg-verde">
                  <h2 class="semilla-nombre">HV-313</h2><!---->
                </div>
              </div>
              <div class="row cultivo-item">
                <div class="col-md-9">
                  <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/HV-313-catalogo-2016.jpg" alt="Maiz H-313" />
                  <div class="row">
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Variedad blancocremoso.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris dos">
                      <p>Resistente a sequías.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Ciclo intermedio/ precoz. </p>
                    </div>
                    <div class="col-md-3 col-sm-6 bg-gris dos">
                      <p>Soporta alta densidad de población y grano pesado.</p>
                    </div>
                    <div class="col-md-3 col-sm-6 bg-gris uno">
                      <p>Se recomienda de 0-1900 m.s.n.m.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <img src="images/mapas/MAIZ BLANCO H313.png" class="img-responsive" alt="Mapa Maiz H-313" />
                  <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Edo. de México, Guerrero, Jalisco, Michoacán, Oaxaca, San Luis Potosí, Sinaloa, Tamaulipas y Zacatecas.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="site-wrapper full">
          <div class="site-wrapper-inner">
            <div class="container">
              <div class="row">
                <div class="col-md-12 bg-verde">
                  <h2 class="semilla-nombre">CAFIME</h2><!---->
                </div>
              </div>
              <div class="row cultivo-item">
                <div class="col-md-9">
                  <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/CAFIME-catalogo-2016.jpg" alt="Maiz CAFIME" />
                  <div class="row">
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Variedad blancocremoso.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris dos">
                      <p>Para temporal y punta de riego.</p>
                    </div>
                    <div class="col-md-2 col-sm-4 bg-gris uno">
                      <p>Buena adaptabilidad de 1000-2500 m.s.n.m.</p>
                    </div>
                    <div class="col-md-3 col-sm-6 bg-gris dos">
                      <p>Excelente respuesta para zonas de baja precipitación y erráticos.</p>
                    </div>
                    <div class="col-md-3 col-sm-6 bg-gris uno">
                      <p>Precoz  (110 días).</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <img src="images/mapas/MAIZ BLANCO CAFIME.png" class="img-responsive" alt="Mapa Maiz CAFIME" />
                  <p class="text-left"><strong>Para los estados de </strong> <br>Altiplano de Aguascalientes, Durango, Oaxaca, Guerrero, Michoacán, Jalisco, San Luís Potosí y Zacatecas.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="site-wrapper full">
          <div class="site-wrapper-inner">
            <h1 class="categoria franjaPadding" id="maiz-amarillos">Maices Amarillos</h1>
            <a href="#">Descargar PDF <img src="images/cultivos/icon_pdf.png" alt="Descargar catálogo de maices amarillos" width="32" height="40" /></a>
            <div class="container franjaPadding">
              <div class="cultivo-item">
                <div class="row">
                  <div class="bg-verde">
                    <h2 class="semilla-nombre">H-384A</h2>
                  </div>
                  <div class="col-md-9">
                    <img  data-wow-duration="0.75s" class="img-responsive wow slideInLeft" src="images/cultivosjpg/H-384A-catalogo-2016.jpg" alt="Maiz H-384A" />
                    <div class="row">
                      <div class="col-md-2 col-sm-4 bg-gris uno">
                        <p>Híbrido amarillo semicristalino.</p>
                      </div>
                      <div class="col-md-2 col-sm-4 bg-gris dos">
                        <p>Ciclo intermedio.</p>
                      </div>
                      <div class="col-md-2 col-sm-4 bg-gris uno">
                        <p>Adaptación en regiones hubicadas entre los 800-1900 m.s.n.m.</p>
                      </div>
                      <div class="col-md-2 col-sm-6 bg-gris dos">
                        <p>Tolerante al acame y buena sanidad de planta.</p>
                      </div>
                      <div class="col-md-4 col-sm-6 bg-gris uno">
                        <p>Con potencial de rendimiento alto y buenas características agronómicas.</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="text-center"><!---->
                    <img src="images/mapas/MAIZ AMARILLO 384A.png" class="img-responsive" alt="Mapa Maiz H-384A" />
                    <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Edo. México, Guerrero,  Jalisco, Oaxaca, San Luis Potosí, Michoacán, Chiapas, Sinaloa, entre otros.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">H-386A</h2>
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/H-386A-catalogo-2016.jpg" alt="Maiz H-386A" />
                <div class="row">
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p>Híbrido amarillo semicristalino.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris dos">
                    <p>Ciclo intermedio.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p>Excelente comportamiento en temporal y riego.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris dos">
                    <p>Tolerante al acame y buena sanidad de planta y mazorca.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p>Recomendable para zonas hubicadas entre 800 - 1900 m.s.n.m.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris dos">
                    <p>Adecuado para utilizarse para grano y forraje.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/MAIZ AMARILLO 386A.png" class="img-responsive" alt="Mapa Maiz H-386A" />
                <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Chiapas, Edo. México, Guerrero, Jalisco, Oaxaca, San Luis Potosí, Michoacán, Sinaloa, entre otros.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">ALMIRANTE</h2><!---->
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/ALMIRANTE-catalogo-2016.jpg" alt="Maiz ALMIRANTE" />
                <div class="row">
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p>Variedad amarillo semicristalino.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris dos">
                    <p>Alto contenido de proteínas y almidones.</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p>Excelente en la dieta  de animales y para uso industrial.</p>
                  </div>
                  <div class="col-md-3 col-sm-6 bg-gris dos">
                    <p>Ciclo intermedio/ precoz (125-135 dias a madurez fisiológica).</p>
                  </div>
                  <div class="col-md-3 col-sm-6 bg-gris uno">
                    <p>Se recomienda 1000-2500 m.s.n.m.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/MAIZ AMARILLO ALMIRANTE.png" class="img-responsive" alt="Mapa Maiz ALMIRANTE" />
                <p class="text-left"><strong>Para los estados de </strong> <br>Colima, Chiapas, Edo. México, Guerrero, Jalisco, Oaxaca, San Luis Potosí, Michoacán, Sinaloa, entre otros.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="trigo">
        <div class="col-md-12 franja wow pulse">
          <h1>TRIGO <span> (Triticum aestivum)</span> <a href="#">Descargar PDF <img src="images/cultivos/icon_pdf.png" alt="Descargar catálogo de semillas de trigo" width="32" height="40" /></a></h1>
        </div>
      </div>
      <div class="back">
        <div class="site-wrapper hidden-xs">
          <div class="site-wrapper-inner portada-trigo wow fadeIn parallax" data-stellar-background-ratio="0">
            <div class="cover-container">
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row franjaPadding">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">CORTÁZAR</h2><!---->
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/Trigo-hdr.jpg" alt="TRIGO CORTÁZAR" />
                <div class="row">
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p><strong>Dias a floración:</strong> 60-70</p>
                  </div>
                  <div class="col-md-2 col-sm-8 bg-gris dos">
                    <p><strong>Potencial de rendimiento:</strong> Excelente de 5-7.5 Ton/ha. Según el manejo.</p>
                  </div>
                  <div class="col-md-1 col-sm-4 bg-gris uno">
                    <p><strong>Altura  de planta (cm):</strong> 95</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris dos">
                    <p><strong>Tolerancia al acame:</strong> Tolerante</p>
                  </div>
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p><strong>Dias a madurez fisiológica:</strong> 104-120</p>
                  </div>
                  <div class="col-md-2 col-sm-6 bg-gris dos">
                    <p><strong>Resistencia a enfermedades:</strong> Excelente</p>
                  </div>
                  <div class="col-md-1 col-sm-6 bg-gris uno">
                    <p><strong>Tipo de gluten:</strong> Suave</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/TRIGO CORTAZAR.png" class="img-responsive" alt="Mapa TRIGO CORTÁZAR" />
                <p class="text-left"><strong>Para los estados de </strong> <br>Jalisco, Michoacán, Zacatecas, San Luis Potosí, Guanajuato y regiones similares.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="avena">
        <div class="col-md-12 franja wow pulse">
          <h1>AVENA <span> (Avena Sativa l.)</span> <a href="#">Descargar PDF <img src="images/cultivos/icon_pdf.png" alt="Descargar catálogo de semillas de trigo" width="32" height="40" /></a></h1>
        </div>
      </div>
      <div class="back">
        <div class="site-wrapper hidden-xs">
          <div class="site-wrapper-inner portada-avena wow fadeIn parallax" data-stellar-background-ratio="0">
            <div class="cover-container">
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row franjaPadding">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">CHIHUAHUA</h2><!---->
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/Avena-hdr.jpg" alt="Mapa AVENA CHIHUAHUA" />
                <div class="row">
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p><strong>Porte:</strong> 90 -150 cm.</p>
                  </div>
                  <div class="col-md-3 col-sm-8 bg-gris dos">
                    <p><strong>Color de grano:</strong> Grande blanco ligeramente pubescente  en la base.</p>
                  </div>
                  <div class="col-md-2 col-sm-6 bg-gris uno">
                    <p><strong>Dias a cosecha:</strong> 130 – 135</p>
                  </div>
                  <div class="col-md-2 col-sm-6 bg-gris dos">
                    <p><strong>Dias a floracion:</strong> 75 – 80</p>
                  </div>
                  <div class="col-md-3 col-sm-6 bg-gris uno">
                    <p><strong>Ciclo:</strong> Intermedio – tardío según condiciones de crecimiento</p>
                  </div>
                  <div class="col-md-4 col-sm-6 bg-gris dos">
                    <p><strong>Enfermedades:</strong> Resistente a la roya del tallo.</p>
                  </div>
                  <div class="col-md-2 col-sm-2 bg-gris uno">
                    <p><strong>Uso:</strong> Forrajero y para grano.</p>
                  </div>
                  <div class="col-md-2 col-sm-2 bg-gris dos">
                    <p><strong>Dias a madurez:</strong> 95 – 110</p>
                  </div>
                  <div class="col-md-4 col-sm-8 bg-gris uno">
                    <p><strong>Adaptacion:</strong> Amplio rango de adaptación en las regiones aveneras de méxico.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/AVENA CHIHUAHUA.png" class="img-responsive" alt="Mapa AVENA CHIHUAHUA" />
                <p class="text-left"><strong>Para los estados de </strong> <br>Jalisco, Michoacán, Zacatecas, San Luis Potosí y regiones similares.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">AVEMEX</h2><!---->
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/Avena-hdr.jpg" alt="AVENA AVEMEX" />
                <div class="row">
                  <div class="col-md-2 col-sm-4 bg-gris uno">
                    <p><strong>Porte:</strong> 90 -150 cm.</p>
                  </div>
                  <div class="col-md-3 col-sm-8 bg-gris dos">
                    <p><strong>Color de grano:</strong> Grande blanco ligeramente pubescente en la base.</p>
                  </div>
                  <div class="col-md-2 col-sm-6 bg-gris uno">
                    <p><strong>Dias a cosecha:</strong> 140 - 150</p>
                  </div>
                  <div class="col-md-2 col-sm-6 bg-gris dos">
                    <p><strong>Dias a floracion:</strong> 75 – 80</p>
                  </div>
                  <div class="col-md-3 col-sm-6 bg-gris uno">
                    <p><strong>Ciclo:</strong> Intermedio – tardío según condiciones de crecimiento</p>
                  </div>
                  <div class="col-md-3 col-sm-6 bg-gris dos">
                    <p><strong>Enfermedades:</strong> Resistente a la roya del tallo.</p>
                  </div>
                  <div class="col-md-2 col-sm-2 bg-gris uno">
                    <p><strong>Uso:</strong> Forrajero y par grano.</p>
                  </div>
                  <div class="col-md-2 col-sm-2 bg-gris dos">
                    <p><strong>Dias a madurez:</strong> 95 – 110</p>
                  </div>
                  <div class="col-md-5 col-sm-8 bg-gris uno">
                    <p><strong>Adaptacion:</strong> Amplio rango de adaptación en las regiones aveneras de méxico.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/AVENA AVEMEX.png" class="img-responsive" alt="Mapa AVENA AVEMEX" />
                <p class="text-left"><strong>Para los estados de </strong> <br>Jalisco, Michoacán, Zacatecas, San Luis Potosí y regiones similares.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="cartamo">
        <div class="col-md-12 franja wow pulse">
          <h1>CÁRTAMO <span> (Carthamus tinctorius l.)</span> <a href="#">Descargar PDF <img src="images/cultivos/icon_pdf.png" alt="Descargar catálogo de semillas de trigo" width="32" height="40" /></a></h1>
        </div>
      </div>
      <div class="back">
        <div class="site-wrapper hidden-xs">
          <div class="site-wrapper-inner portada-cartamo wow fadeIn parallax" data-stellar-background-ratio="0">
            <div class="cover-container">
            </div>
          </div>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner">
          <div class="container">
            <div class="row franjaPadding">
              <div class="col-md-12 bg-verde">
                <h2 class="semilla-nombre">BACUM</h2><!---->
              </div>
            </div>
            <div class="row cultivo-item">
              <div class="col-md-9">
                <img  data-wow-duration="0.75s" class="wow slideInLeft img-responsive" src="images/cultivosjpg/Cartamo-hdr.jpg" alt="CÁRTAMO BACUM" />
                <div class="row">
                  <div class="col-md-3 col-sm-3 bg-gris uno">
                    <p><strong>Espigamiento:</strong> 80-90 dias.</p>
                  </div>
                  <div class="col-md-2 col-sm-3 bg-gris dos">
                    <p><strong>Altura:</strong> 120 -150 cm.</p>
                  </div>
                  <div class="col-md-3 col-sm-3 bg-gris uno">
                    <p><strong>Profundidad de siembra:</strong> Máx. 5 Cm.</p>
                  </div>
                  <div class="col-md-4 col-sm-3 bg-gris dos">
                    <p><strong>Tipo de siembra:</strong> Humedad residual y riego</p>
                  </div>
                  <div class="col-md-4 col-sm-3 bg-gris uno">
                    <p><strong>Ciclo vegetativo:</strong> Intermedio – tardío</p>
                  </div>
                  <div class="col-md-4 col-sm-3 bg-gris dos">
                    <p><strong>Semilla necesaria:</strong>  De 20 – 25 kg./Ha.</p>
                  </div>
                  <div class="col-md-2 col-sm-3 bg-gris uno">
                    <p><strong>Suceptible:</strong> Ph roya y alternaria.</p>
                  </div>
                  <div class="col-md-2 col-sm-3 bg-gris dos">
                    <p><strong>Se recomienda en:</strong> 95 – 110</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <img src="images/mapas/CARTAMO BACUM.png" class="img-responsive" alt="Mapa CÁRTAMO BACUM" />
                <p class="text-left"><strong>Se recomienda en </strong> <br>Zona ‘Ciénega’ de Jalisco.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="Presencia">
        <div class="col-md-12 franja wow pulse">
          <h1>Presencia</h1>
        </div>
      </div>
      <div class="contacto-etiqueta Presencia">
        <div class="container">
          <p><strong>IYADILPRO</strong> tiene cobertura nacional en 11 estados, haciendo atractivos negocios con nuestros distribuidores autorizados.</p>
        </div>
      </div>
      <div class="site-wrapper franjaVerde">
        <div class="site-wrapper-inner portada-Presencia wow fadeIn parallax" data-stellar-background-ratio="0">
          
          <div class="franjaVerde">
            <div class="container">
              <div class="col-md-12">
                <img src="images/mapas/MAIZ AMARILLO 386A.png" class="img-responsive marginTopBottom" alt="Presencia Semillas Iyadilpro en México" />
              </div>
              
            </div>
            <div class="container">
              <div class="col-md-3 col-sm-6">
                <img  data-wow-duration="0.25s" class="wow slideInUp img-responsive" src="images/presencia/franja/maiz1.jpg" alt="MAÍZ Semillas Iyadilpro" />
                <h2 class="incrustado">MAÍZ</h2>
              </div>
              <div class="col-md-3 col-sm-6">
                <img  data-wow-duration="0.55s" class="wow slideInUp img-responsive" src="images/presencia/franja/avena1.jpg" alt="AVENA Semillas Iyadilpro" />
                <h2 class="incrustado">AVENA</h2>
              </div>
              <div class="col-md-3 col-sm-6">
                <img  data-wow-duration="0.85s" class="wow slideInUp img-responsive" src="images/presencia/franja/cartamo1.jpg" alt="CÁRTAMO Semillas Iyadilpro" />
                <h2 class="incrustado">CÁRTAMO</h2>
              </div>
              <div class="col-md-3 col-sm-6">
                <img  data-wow-duration="1.15s" class="wow slideInUp img-responsive" src="images/presencia/franja/trigo1.jpg" alt="TRIGO Semillas Iyadilpro" />
                <h2 class="incrustado">TRIGO</h2>
              </div>
            </div>
            <div class="traslucido">
              <div class="row">
                <div class="container">
                  <div class="col-md-3 col-sm-6">
                    <ul class="Presencia-list">
                      <li class="li-header"><strong>BLANCO</strong></li>
                      <li>H-377</li>
                      <li>H-318</li>
                      <li>HV-313</li>
                      <li>CAFIME</li>
                      <li class="li-header"><strong>AMARILLO</strong></li>
                      <li>H-384A</li>
                      <li>H-386A</li>
                      <li>ALMIRANTE</li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <ul class="Presencia-list">
                      <li>CHIHUAHUA</li>
                      <li>AVEMEX</li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <ul class="Presencia-list">
                      <li>BACUM</li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <ul class="Presencia-list">
                      <li>CORTÁZAR</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-6">
                    <p class="lead naranja">¿NECESITAS  UNA COTIZACIÓN? </p>
                    <p class="lead verde">¿TE INTERESA  UNA CONCESIÓN?</p>
                    <p class="well">Por favor comunícate con nosotros, al área de ventas y con gusto te atenderemos.</p>
                    <div class="Presencia-etiqueta wow animated slideInLeft" data-wow-duration="0.55s">
                      <span><i class="fa fa-phone"></i></span>
                      <p><strong>TELÉFONOS:</strong> 01 (392) 92 401 86<br/>01 (392) 92 411 00<br/>01 (392) 92 416 20<br/><strong>FAX:</strong> 01 (392) 392 416 18</p>
                    </div>
                    <div class="Presencia-etiqueta wow slideInLeft" data-wow-duration="0.85s">
                      <span><i class="fa fa-envelope"></i></span>
                      <p><strong>E-MAIL:</strong><br>iyadilpro@prodigy.net.mx</p>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="logoCenter hidden-xs hidden-sm marginTopBottom">
                      <img class="animated pulse img-responsive" src="images/logo160x315negro.png" alt="Semillas Iyadilpro y Ya Logo">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="galeria">
        <div class="col-md-12 franja wow pulse">
          <h1>GALERÍA</h1>
        </div>
      </div>
      
      <div class="site-wrapper full">
        <div class="portada-galeria wow fadeIn fill parallax" data-stellar-background-ratio="0">
          <div class="container">
            <div class="row">
              
              <!-- tabs right -->
              <div class="tabbable">
                <div class="col-sm-3 col-sm-push-9">
                  <ul class="nav nav-pills nav-stacked" >
                    <h3>CATEGORÍAS</h3>
                    <br>
                    <?php
                    include("conexion.php");
                    $sql2 = mysql_query("SELECT id,titulo FROM `galerias`");
                    $cont = 0;
                    while($row2 = mysql_fetch_array($sql2)){
                    echo '<li class="'.(($cont===0)?'active':'').'"><a href="#gal_'.utf8_decode($row2[0]).'" data-toggle="tab" class="">'.utf8_decode($row2[1]).'</a></li>';
                    $cont++;
                    }
                    ?>
                  </ul>
                </div>
                <div class="tab-content col-sm-9  col-sm-pull-3">
                  <?php
                  $sql2 = mysql_query("SELECT id,titulo FROM `galerias`");
                  $cont2 = 0;
                  while($row2 = mysql_fetch_array($sql2)){
                  echo '<div class="tab-pane '.(($cont2===0)?'active':'').'" id="gal_'.utf8_decode($row2[0]).'">';
                    echo '  <div class="row">';
                      $cont++;
                      $sql = mysql_query("SELECT id,title,description,idGal,src,fecha FROM `imagenes` WHERE idGal = ".$row2[0]." order by idGal");
                      while($row = mysql_fetch_array($sql)){
                      $fecha = explode(" ",$row[5]);
                      echo '<div class="col-md-4 col-sm-4 opcion">';
                        echo '<a class="image-popup-vertical-fit" href="'.utf8_decode($row[4]).'" title="'.utf8_decode($row[1]).'">';
                          echo '<img class="wow fadeIn img-responsive" src="'.utf8_decode($row[4]).'" alt="'.utf8_decode($row[2]).'">';
                        echo '</a>';
                        echo '<p>'.utf8_decode($row[1]).' <i>/ '.$fecha[0].'</i></p>';
                      echo "</div>";
                      }
                    echo '  </div>';
                  echo '</div>';
                  }
                  ?>
                </div>
              </div>
              <!-- /tabs -->
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="contacto">
        <div class="col-md-12 franja wow animated pulse">
          <h1>CONTACTO</h1>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner portada-contacto">
          <div class="container">
            <div class="row franjaPadding">
              <div class="contacto-etiqueta wow animated slideInLeft" data-wow-duration="0.25s">
                <span><i class="fa fa-map"></i> </span>
                <p><strong>DIRECCIÓN:</strong><br>Carretera Jamay – La Barca, Km. 5, Jamay Jalisco, México.</p>
              </div>
              <div class="contacto-etiqueta wow animated slideInLeft" data-wow-duration="0.55s">
                <span><i class="fa fa-phone"></i> </span>
                <p><strong>TELÉFONOS:</strong><br>01 (392) 92 401 86 - 01 (392) 92 411 00 - 01 (392) 92 416 20 - FAX: 01 (392) 392 4 16 18</p>
              </div>
              <div data-wow-duration="0.85s" class="contacto-etiqueta wow animated slideInLeft" type="button" data-toggle="collapse" data-target="#emailForm" aria-expanded="true" aria-controls="collapseExample">
                <span><i class="fa fa-envelope"></i> </span>
                <p><strong>E-MAIL:</strong><br>iyadilpro@prodigy.net.mx</p>
              </div>
              <div class="collapse in" id="emailForm">
                <div data-wow-duration="1.45s" data-wow-delay="0.25s" class="wow animated tada">
                  <form class="form-horizontal well">
                    <h2><strong>CONTÁCTANOS: </strong></h2>
                    <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" placeholder="Tu email" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="mensaje" class="col-sm-2 control-label">Mensaje</label>
                      <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="mensaje" placeholder="Tu mensaje" required></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id="enviarEmail" class="btn btn-default pull-right">Enviar mensaje</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row franjaPadding" id="ubicacion">
        <div class="col-md-12 franja wow animated pulse">
          <h1>UBICACIÓN</h1>
        </div>
      </div>
      <div class="site-wrapper full">
        <div class="site-wrapper-inner portada-ubicacion">
          <div class="container">
            <div class="row franjaPadding">
              <div class="embed-responsive embed-responsive-16by9 mapa hidden-xs">
                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29939.47744598128!2d-102.64811760062473!3d20.28226723984899!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842edd007018248f%3A0x8e67e9dce83e669d!2sSemillas+Iyadilpro+y+Ya!5e0!3m2!1ses-419!2smx!4v1458421176837" style="border:0" allowfullscreen></iframe>
              </div>
              <div class="contacto-etiqueta">
                <span><i class="fa fa-map"></i></span>
                <p><strong>DIRECCIÓN:</strong><br>Carretera Jamay – La Barca, Km. 5, Jamay Jalisco, México. <a class="btn btn-default visible-xs" href="https://www.google.com.mx/maps/place/Semillas+Iyadilpro+y+Ya/@20.286685,-102.6726386,13z/data=!4m6!1m3!3m2!1s0x842edd007018248f:0x8e67e9dce83e669d!2sSemillas+Iyadilpro+y+Ya!3m1!1s0x842edd007018248f:0x8e67e9dce83e669d?hl=es-419">Ver mapa en Google Maps</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <p id="go-top" class="pull-right hidden-xs animated"><a href="#inicio">Ir Arriba</a></p>
      <div class="franjaPadding">
        <div class="row">
          <div class="mastfoot">
            <p class="text-center"><a href="http://www.semillasiyadilpro.com">Semillas Iyadilpro 2016</a></p>
          </div>
        </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
      <script src="bootstrap/bootstrap.min.js"></script>
      <script src="js/modernizr-2.6.2.min.js"></script>
      <script src="js/jquery.stellar.js"></script>
      <script src="js/wow.min.js"></script>
      <script src="js/jquery.magnific-popup.min.js"></script>
      <script>
      
        $(document).ready(function() {
         $('#navbar').find('li').find('a').on('click', function(e) {
             e.preventDefault();
             $('.navbar-collapse').collapse('hide');
         });
         var wow = new WOW({
             boxClass: 'wow', // animated element css class (default is wow)
             animateClass: 'animated', // animation css class (default is animated)
             offset: 100, // distance to the element when triggering the animation (default is 0)
             mobile: true, // trigger animations on mobile devices (default is true)
             live: true, // act on asynchronously loaded content (default is true)
             callback: function(box) {},
             scrollContainer: null // optional scroll container selector, otherwise use window
         });
         wow.init();
         $('.carousel').carousel();
         $.fn.extend({
             animateCss: function(animationName) {
                 var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                 $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                     $(this).removeClass('animated ' + animationName);
                 });
             }
         });
         $('.image-popup-vertical-fit').magnificPopup({
             type: 'image',
             closeOnContentClick: true,
             mainClass: 'mfp-img-mobile',
             image: {
                 verticalFit: true
             }
         });
         linkInterno = $('a[href^="#"]:not(a[href^="#gal_"])');
         linkInterno.on('click', function(e) {
             e.preventDefault();
             var href = $(this).attr('href');
             $('html, body').animate({
                 scrollTop: $(href).offset().top
             }, 'slow', 'easeInOutExpo');
         });
         $(window).scroll(function() {
             var $window = $(window),
                 //win_height_padded = $window.height() - 50;
                 //win_height_padded = screen.availHeight - 126;
                 win_height_padded = screen.availHeight - 176;
             isTouch = Modernizr.touch;
             if ($(this).scrollTop() === 0) {
                 //$('nav').hide();
             }
             if ($(this).scrollTop() > win_height_padded) {
                 $('#navbar').removeClass("animated visible-xs fadeOut").addClass("animated fadeIn").show();
                 $('#go-top').removeClass("fadeOut").addClass("zoomInUp").show();
             } else {
                 if ($(this).scrollTop() > 1) {
                     $('navbar').removeClass("animated fadeIn").addClass("animated visible-xs fadeOut");
                     $('#go-top').removeClass("zoomInUp").addClass("fadeOut");
                 }
             }
         });

         $(function() {
             $(window).stellar();
             $('.wrapper').css({
                 'height': screen.availHeight + 'px'
             });
         });
         $('#enviarEmail').on("click", function(e) {
             e.preventDefault();
             var dataSend = {
                 email: $('#email').val(),
                 mensaje: $('#mensaje').val()
             }
             $.ajax({
                 url: 'enviarEmail.php',
                 data: dataSend,
                 success: function(data) {
                     alert("Correo enviado exitosamente.");
                     $('#email').val('');
                     $('#mensaje').val('');
                 }
             });
         });
         /* Scroll event handler */
         $(window).bind('scroll', function(e) {
             //parallaxScroll();
             redrawDotNav();
         });

         /* Show/hide dot lav labels on hover */
         $('nav#primary a').hover(
             function() {
                 $(this).prev('h1').show();
             },
             function() {
                 $(this).prev('h1').hide();
             }
         );
         redrawDotNav();

         function redrawDotNav() {
             var inicio = $('#inicio');
             var navbar = $('#navbar');
             var myCarousel = $('#myCarousel');
             var opciones = $('#opciones');
             var quienessomos = $('#quienessomos');
             var cultivos = $('#cultivos');
             var maices = $('#maices');
             var maizblancos = $('#maiz-blancos');
             var maizamarillos = $('#maiz-amarillos');
             var trigo = $('#trigo');
             var avena = $('#avena');
             var cartamo = $('#cartamo');
             var Presencia = $('#Presencia');
             var galeria = $('#galeria');
             var contacto = $('#contacto');
             var ubicacion = $('#ubicacion');
             var section1Top = 0;
             // The top of each section is offset by half the distance to the previous section.
             var section2Top = inicio.offset().top - ((quienessomos.offset().top - inicio.offset().top) / 2);
             var section3Top = quienessomos.offset().top - ((cultivos.offset().top - quienessomos.offset().top) / 2);
             var section4Top = cultivos.offset().top - ((Presencia.offset().top - cultivos.offset().top) / 2);
             var section5Top = Presencia.offset().top - ((galeria.offset().top - Presencia.offset().top) / 2);
             var section6Top = galeria.offset().top - ((contacto.offset().top - galeria.offset().top) / 2);
             var section7Top = contacto.offset().top - ((ubicacion.offset().top - contacto.offset().top) / 2);
             var section8Top = ubicacion.offset().top - (($(document).height() - ubicacion.offset().top) / 2);
             $('nav#primary a').removeClass('active');
             if ($(document).scrollTop() >= section2Top && $(document).scrollTop() < section3Top) {
                 console.log("section2Top");
                 $('nav#primary a.inicio').addClass('active');
             } else if ($(document).scrollTop() >= section3Top && $(document).scrollTop() < section4Top) {
                 console.log("section3Top");
                 $('nav#primary a.quienessomos').addClass('active');
             } else if ($(document).scrollTop() >= section4Top && $(document).scrollTop() < section5Top) {
                 console.log("section4Top");
                 $('nav#primary a.cultivos').addClass('active');
             } else if ($(document).scrollTop() >= section5Top && $(document).scrollTop() < section6Top) {
                 console.log("section5Top");
                 $('nav#primary a.Presencia').addClass('active');
             } else if ($(document).scrollTop() >= section6Top && $(document).scrollTop() < section7Top) {
                 console.log("section6Top");
                 $('nav#primary a.galeria').addClass('active');
             } else if ($(document).scrollTop() >= section7Top && $(document).scrollTop() < section8Top) {
                 console.log("section7Top");
                 $('nav#primary a.contacto').addClass('active');
             } else if ($(document).scrollTop() >= section8Top) {
                 console.log("section8Top");
                 $('nav#primary a.ubicacion').addClass('active');
             }
          }
        });
      </script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="js/ie10-viewport-bug-workaround.js"></script>
      <script src="js/jquery.nicescroll.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    </body>
  </html>