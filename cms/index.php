<?php 
	session_start();

	if(isset($_SESSION['user_id'])){


    include('../configuracion.php');
    include('../conexion.php');



    if (@$_GET['id']!=="" &&   @$_GET['accion']==="eliminar") {


      if (isset($_GET['modulo'])) {


        if ($_GET['modulo'] === "imagenes") {

          if (isset($_GET['accion'])) {

            if ($_GET['accion'] === "eliminar") {

              $sql = mysql_query("SELECT src FROM `imagenes` WHERE id = ".$_GET['id']);
              $row = mysql_fetch_row($sql);
              unlink("../".$row[0]);


              $sql = mysql_query("DELETE FROM imagenes WHERE id = ".$_GET['id']."");
              if ($sql) {
                 echo '<script>alert("Imagen eliminada correctamente");  window.history.back(); </script>';
              }


            }


          }
          
        }

      }
    }


?>	


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE?>images/favicon.png">
    <title>Panel de control - Semillas Iyadilpro y Ya</title>
    <!-- Bootstrap core CSS -->
    <link href="<?= BASE?>bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= BASE?>css/font-awesome.min.css">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?= BASE?>css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= BASE?>css/cover.css" rel="stylesheet">
    <link href="<?= BASE?>cms/cms.css" rel="stylesheet">
    <link href="<?= BASE?>css/animate.css" rel="stylesheet">
    <link href="<?= BASE?>css/magnific-popup.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?= BASE?>js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

        <div class="site-wrapper" id="inicio">
          <div class="site-wrapper-inner portada-prescencia animated fadeIn fill">
            <nav class="navbar navbar-fixed-top navbar-default">
              <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand hidden-sm" href="#inicio">Semillas Iyadilpro</a>
                  <a class="navbar-brand visible-sm" href="#inicio"><img class="animated pulse img-responsive" src="<?= BASE?>images/favicon.png" alt="Semillas Iyadilpro y Ya"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Galeria <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#imagenes">Cargar imagenes</a></li>
                        <li><a href="#galerias">Administrar galerias</a></li>
                      </ul>
                    </li>
                  </ul>
                </div><!--/.nav-collapse -->
              </div><!--/.container-fluid -->
            </nav>
            
          </div>
        </div>



        <div class="row franjaPadding" id="galerias">
          <div class="col-md-12 franja wow animated pulse">
            <h1>ADMINISTRAR GALERIAS</h1>
          </div>
        </div>
        <div class="site-wrapper">
          <div class="site-wrapper-inner portada-trigo wow fadeIn parallax" data-stellar-background-ratio="0.2">
            <div class="container">

              <div class="collapse in" id="emailForm">
                <div data-wow-duration="1.45s" data-wow-delay="0.25s" class="wow animated tada">

                  <form class="form-horizontal well" action="cms_action.php" enctype="multipart/form-data" method="post">

                    <input type="hidden" name="modulo" value="galerias" />
                    <input type="hidden" name="accion" value="crear" />

                    <h2><strong>Información de Galeria: </strong></h2>
                    <div class="form-group">
                      <label for="titulo" class="col-sm-2 control-label">Título de la galeria</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título de la galeria" autocomplete="off" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id="galeriaSubmit" name="submit" class="btn btn-default pull-right">Crear galeria</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>




        <div class="row franjaPadding" id="imagenes">
          <div class="col-md-12 franja wow animated pulse">
            <h1>CARGA DE IMAGENES</h1>
          </div>
        </div>
        <div class="site-wrapper">
          <div class="site-wrapper-inner portada-avena wow fadeIn parallax" data-stellar-background-ratio="0.2">
            <div class="container">

              <div class="collapse in" id="emailForm">
                <div data-wow-duration="1.45s" data-wow-delay="0.25s" class="wow animated tada">

                  <form class="form-horizontal well" action="cms_action.php" enctype="multipart/form-data" method="post">

                  	<input type="hidden" name="modulo" value="imagenes" />
                  	<input type="hidden" name="accion" value="carga" />

                    <h2><strong>Información de Imagen: </strong></h2>
                    <div class="form-group">
                      <label for="titulo" class="col-sm-2 control-label">Título</label>
                      <div class="col-sm-10">
                        <input autocomplete="off" type="text" class="form-control" id="titulo" name="titulo" placeholder="Título de la imagen" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
                      <div class="col-sm-10">
                        <textarea autocomplete="off" type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción de la imagen" required></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion" class="col-sm-2 control-label">Galeria</label>
                      <br>
                      <div class="col-sm-10">
                        <select name="galeria">
                        	<option value="">Selecciona..</option>
                          <?php 
                            $sql = mysql_query("SELECT id,titulo FROM galerias");

                            while($row = mysql_fetch_array($sql)){
                              echo "<option value='".$row[0]."'>".utf8_decode($row[1])."</option>";
                            }
                           ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion" class="col-sm-2 control-label">Imagen</label>
                      <div class="col-sm-10">
                        <input autocomplete="off" type="file" class="form-control" name="imagen" id="imagen" placeholder="Título de la imagen" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id="imagenSubmit" name="submit" class="btn btn-default pull-right">Cargar imagen</button>
                      </div>
                    </div>


                    <h1>Galeria</h1>
                    <div>
                      <div>   
                            <?php 
                              $sql2 = mysql_query("SELECT id,titulo FROM `galerias`");
                              while($row2 = mysql_fetch_array($sql2)){
                                echo "<h3>".utf8_decode($row2[1])."</h3>";
                                echo '<ul class="list-inline"> ';
                                $sql = mysql_query("SELECT id,title,description,idGal,src FROM `imagenes` WHERE idGal = ".$row2[0]." order by idGal");

                                while($row = mysql_fetch_array($sql)){
                                  echo "<li style='margin:5px;'><img src='../".utf8_decode($row[4])."' class='img-responsive' width='100' />";
                                  echo "<p><strong>".utf8_decode($row[1])."</strong></p>";
                                  echo "<p>".utf8_decode($row[2])."</p>";
                                  echo '<a href="?accion=eliminar&modulo=imagenes&id='.utf8_decode($row[0]).'" class="btn btn-danger btn-xs">X</a>';
                                  echo "</li>";
                                }
                                echo '</ul>';
                              }  
                            ?>
                      </div>
                    </div>




                  </form>




                </div>
              </div>


            </div>
          </div>
        </div>

        <p id="go-top" class="pull-right hidden-xs animated"><a href="#inicio">Ir Arriba</a></p>
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?= BASE?>js/jquery.min.js"><\/script>')</script>
        <script src="<?= BASE?>bootstrap/bootstrap.min.js"></script>
        <script src="<?= BASE?>js/modernizr-2.6.2.min.js"></script>
        <script src="<?= BASE?>js/jquery.stellar.js"></script>
        <script src="<?= BASE?>js/wow.min.js"></script>
        <script src="<?= BASE?>js/jquery.magnific-popup.min.js"></script>
        <script>
        
          $(document).ready(function() {

              var wow = new WOW({
                  boxClass: 'wow', // animated element css class (default is wow)
                  animateClass: 'animated', // animation css class (default is animated)
                  offset: 100, // distance to the element when triggering the animation (default is 0)
                  mobile: true, // trigger animations on mobile devices (default is true)
                  live: true, // act on asynchronously loaded content (default is true)
                  callback: function(box) {
                  },
                  scrollContainer: null // optional scroll container selector, otherwise use window
              });
              wow.init();

              $('.carousel').carousel();

              $.fn.extend({
                  animateCss: function(animationName) {
                      var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                      $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                          $(this).removeClass('animated ' + animationName);
                      });
                  }
              });

              /*
              $('.opcion').on("mouseenter", function() {
                  $(this).animateCss('pulse');
              });

              $('.opcion').on("click", function() {
                  $(this).animateCss('flash');
              });
              */

              $('.image-popup-vertical-fit').magnificPopup({
                  type: 'image',
                  closeOnContentClick: true,
                  mainClass: 'mfp-img-mobile',
                  image: {
                      verticalFit: true
                  }

              });

              linkInterno = $('a[href^="#"]');
              linkInterno.on('click', function(e) {
                  e.preventDefault();
                  var href = $(this).attr('href');
                  $('html, body').animate({
                      scrollTop: $(href).offset().top
                  }, 'slow', 'easeInOutExpo');
              });



              $(window).scroll(function() {

                  var $window           = $(window),
                      //win_height_padded = $window.height() - 50;
                      win_height_padded = screen.availHeight;
                      isTouch           = Modernizr.touch;

                  if ($(this).scrollTop() === 0) {
                      //$('nav').hide();
                  }
                  if ($(this).scrollTop() > win_height_padded) {
                    
                      $('#go-top').removeClass("fadeOut").addClass("zoomInUp").show();
                  } else {
                      if ($(this).scrollTop() > 1) {
                       
                          $('#go-top').removeClass("zoomInUp").addClass("fadeOut");
                      }
                  }
              });


         
              $(function(){
                $(window).stellar(); 
                $('.wrapper').css({
                  'height': screen.availHeight + 'px'
                });
              });



          });

        </script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?= BASE?>js/ie10-viewport-bug-workaround.js"></script>
        <script src="<?= BASE?>js/jquery.nicescroll.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      </html>


<?php 
	} else {

    header('Location: login.php');

  }
 ?>