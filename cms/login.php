<?php 
    session_start();

    if(!isset($_SESSION['user_id'])){
        include('../configuracion.php');
?>  

<!DOCTYPE html>
<html>
  <head>
    <title>Acceso a panel de control Iyadilpro</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--<link rel="shortcut icon" href="<?= BASE?>"/>-->
    <link rel="stylesheet" type="text/css" href="<?= BASE?>bootstrap/bootstrap.min.css">        
    <link rel="stylesheet" type="text/css" href="<?= BASE?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= BASE?>css/toaster.css">
    <link href="<?= BASE?>css/signin.css" rel="stylesheet">        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?= BASE?>js/components/jquery/dist/jquery.min.js"><\/script>')</script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin">
        <h2 class="form-signin-heading">Acceso Iyadilpro</h2>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="email" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <!--<label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>-->
        </div>
        <button class="btn btn-lg btn-primary btn-block" id="logear" type="submit">Accesar</button>
      </form>
    </div>

        <script src="<?= BASE?>bootstrap/bootstrap.min.js"></script>
        <script src="<?= BASE?>js/modernizr-2.6.2.min.js"></script>
        <script src="<?= BASE?>js/jquery.stellar.js"></script>
        <script src="<?= BASE?>js/wow.min.js"></script>
        <script src="<?= BASE?>js/jquery.magnific-popup.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#logear').on('click',function(e){
                    e.preventDefault();

                    var dataSend = {
                      email: $('#email').val(),
                      password: $('#password').val()
                    }
                    $.ajax({
                        url: 'logear.php',
                        data: dataSend,
                        type: "POST",
                        success: function(data) {

                            $('#email').val('');
                            $('#password').val('');
                            if(+data === 1)
                                location.href = "index.php";
                        }
                    });
                });
           });
        </script>
  </body>
</html>
<?php 
    } else {
        header('Location: index.php');
    }
 ?>